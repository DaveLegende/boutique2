/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Personne;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Dave
 */
@Path("/boutique")
public class PersonneService {
    private List<Personne> liste;
    
    public void ajouter(Personne p) {
        if(liste.contains(p)) {
            System.out.println("Le personne existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(Personne p) {
        if(liste.contains(p)) {
            for(Personne personne : liste) {
                if(personne.equals(p)) {
                    personne = p;
                }
            }
        }
        else {
            System.out.println("Le Personne est introuvable");
        }
    }
    
    public Personne trouver(int id) {
        Personne personne = new Personne();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Personne p : liste) {
                if(p.getId() == id) {
                    System.out.println("Personne trouvé");
                    personne = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return personne;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Personne c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Personne e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Personne c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Personne Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Personne> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Personne c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Personne> lister(int debut, int nombre) {
        
        int index = debut;
        List<Personne> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Personne>) maListe.iterator());
    }
}


