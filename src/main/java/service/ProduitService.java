/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Produit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Dave
 */
@Path("/boutique")
public class ProduitService {
    private List<Produit> liste;
    
    public void ajouter(Produit p) {
        if(liste.contains(p)) {
            System.out.println("Le produit existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(Produit p) {
        if(liste.contains(p)) {
            for(Produit produit : liste) {
                if(produit.equals(p)) {
                    produit = p;
                }
            }
        }
        else {
            System.out.println("Le Produit est introuvable");
        }
    }
    
    public Produit trouver(int id) {
        Produit produit = new Produit();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Produit p : liste) {
                if(p.getId() == id) {
                    System.out.println("Produit trouvé");
                    produit = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return produit;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Produit c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Produit e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Produit c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Produit Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Produit> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Produit c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Produit> lister(int debut, int nombre) {
        
        int index = debut;
        List<Produit> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Produit>) maListe.iterator());
    }
}
