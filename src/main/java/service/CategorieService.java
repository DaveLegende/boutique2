/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Categorie;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Dave
 */
public class CategorieService {
    public static List<Categorie> liste;
    
    
    
    // Fonctions
    public void ajouter(Categorie e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Categorie c : liste) {
                if(liste.contains(c)) {
                    System.out.println("Element deja dans la liste");
                }
                else {
                    liste.add(c);
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void modifier(Categorie e) {
        if(liste == null) {
            System.err.println("La Liste est vide");
        }
        else {
            for(Categorie c : liste) {
                if(c.equals(e)) {
                    c = e;
                }
                else {
                    System.out.println("Id non trouvable");
                }
            }
            System.out.println(""+liste);
        }
        
    }
    
    public Categorie trouver(Integer id) {
        Categorie categorie = new Categorie();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Categorie c : liste) {
                if(c.getId() == id) {
                    System.out.println("categorie trouvé");
                    categorie = c;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return categorie;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Categorie c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Categorie e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Categorie c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Categorie Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Categorie> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Categorie c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Categorie> lister(int debut, int nombre) {
        
        int index = debut;
        List<Categorie> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Categorie>) maListe.iterator());
    }
    
    
}
