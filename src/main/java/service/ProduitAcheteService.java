/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.ProduitAchete;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Dave
 */
@Path("/boutique")
public class ProduitAcheteService {
    private List<ProduitAchete> liste;
    
    public void ajouter(ProduitAchete p) {
        if(liste.contains(p)) {
            System.out.println("Le produitAchete existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(ProduitAchete p) {
        if(liste.contains(p)) {
            for(ProduitAchete produitAchete : liste) {
                if(produitAchete.equals(p)) {
                    produitAchete = p;
                }
            }
        }
        else {
            System.out.println("Le ProduitAchete est introuvable");
        }
    }
    
    public ProduitAchete trouver(int id) {
        ProduitAchete produitAchete = new ProduitAchete();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(ProduitAchete p : liste) {
                if(p.getId() == id) {
                    System.out.println("ProduitAchete trouvé");
                    produitAchete = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return produitAchete;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(ProduitAchete c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(ProduitAchete e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(ProduitAchete c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("ProduitAchete Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<ProduitAchete> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(ProduitAchete c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<ProduitAchete> lister(int debut, int nombre) {
        
        int index = debut;
        List<ProduitAchete> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends ProduitAchete>) maListe.iterator());
    }
}



