/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Client;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 *
 * @author Dave
 */
@Path("/boutique")
public class ClientService {
    private List<Client> liste;
    
    public void ajouter(Client p) {
        if(liste.contains(p)) {
            System.out.println("Le client existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(Client p) {
        if(liste.contains(p)) {
            for(Client client : liste) {
                if(client.equals(p)) {
                    client = p;
                }
            }
        }
        else {
            System.out.println("Le Client est introuvable");
        }
    }
    
    public Client trouver(int id) {
        Client client = new Client();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Client p : liste) {
                if(p.getId() == id) {
                    System.out.println("Client trouvé");
                    client = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return client;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Client c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Client e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Client c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Client Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Client> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Client c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Client> lister(int debut, int nombre) {
        
        int index = debut;
        List<Client> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Client>) maListe.iterator());
    }
}
