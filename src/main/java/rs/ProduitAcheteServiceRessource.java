/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.ProduitAchete;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.ProduitAcheteService;

/**
 *
 * @author Dave
 */
@Path("/produitAcheteService")
public class ProduitAcheteServiceRessource {
    ProduitAcheteService paServ = new ProduitAcheteService();
    
    @POST
    @Path("/produitAcheteService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProduitAchete(@PathParam("e") ProduitAchete e) {
        
        paServ.ajouter(e);
    }
    
    @POST
    @Path("/produitAcheteService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateProduitAchete(@PathParam("e") ProduitAchete e) {
        
        paServ.modifier(e);
    }
    
    @GET
    @Path("/produitAcheteService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public ProduitAchete addProduitAchete(@PathParam("id") Integer id) {
        
        return paServ.trouver(id);
    }
    
    @DELETE
    @Path("/produitAcheteService/delete")
    public void deleteProduitAchete(@PathParam("id") Integer id) {
        
        paServ.supprimer(id);
    }
    
    @DELETE
    @Path("/produitAcheteService/delete")
    public void deleteProduitAchete(@PathParam("e") ProduitAchete e) {
        
        paServ.supprimer(e);
    }
    
    @GET
    @Path("/produitAcheteService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<ProduitAchete> getListe() {
        
        return paServ.lister();
    }
    
    @GET
    @Path("/produitAcheteService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<ProduitAchete> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return paServ.lister(debut, nombre);
    }
    
}



