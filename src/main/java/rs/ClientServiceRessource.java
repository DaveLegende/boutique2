/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Client;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.ClientService;

/**
 *
 * @author Dave
 */
@Path("/clientService")
public class ClientServiceRessource {
    ClientService cltServ = new ClientService();
    
    @POST
    @Path("/clientService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addClient(@PathParam("e") Client e) {
        
        cltServ.ajouter(e);
    }
    
    @POST
    @Path("/clientService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateClient(@PathParam("e") Client e) {
        
        cltServ.modifier(e);
    }
    
    @GET
    @Path("/clientService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Client addClient(@PathParam("id") Integer id) {
        
        return cltServ.trouver(id);
    }
    
    @DELETE
    @Path("/clientService/delete")
    public void deleteClient(@PathParam("id") Integer id) {
        
        cltServ.supprimer(id);
    }
    
    @DELETE
    @Path("/clientService/delete")
    public void deleteClient(@PathParam("e") Client e) {
        
        cltServ.supprimer(e);
    }
    
    @GET
    @Path("/clientService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Client> getListe() {
        
        return cltServ.lister();
    }
    
    @GET
    @Path("/clientService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Client> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return cltServ.lister(debut, nombre);
    }
    
}

