/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.CategorieService;

/**
 *
 * @author Dave
 */
@Path("/categorieService")
public class CategorieServiceRessource {
    CategorieService catServ = new CategorieService();
    
    @POST
    @Path("/categorieService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addCategorie(@PathParam("e") Categorie e) {
        
        catServ.ajouter(e);
    }
    
    @POST
    @Path("/categorieService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCategorie(@PathParam("e") Categorie e) {
        
        catServ.modifier(e);
    }
    
    @GET
    @Path("/categorieService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Categorie addCategorie(@PathParam("id") Integer id) {
        
        return catServ.trouver(id);
    }
    
    @DELETE
    @Path("/categorieService/delete")
    public void deleteCategorie(@PathParam("id") Integer id) {
        
        catServ.supprimer(id);
    }
    
    @DELETE
    @Path("/categorieService/delete")
    public void deleteCategorie(@PathParam("e") Categorie e) {
        
        catServ.supprimer(e);
    }
    
    @GET
    @Path("/categorieService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Categorie> getListe() {
        
        return catServ.lister();
    }
    
    @GET
    @Path("/categorieService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Categorie> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return catServ.lister(debut, nombre);
    }
}
