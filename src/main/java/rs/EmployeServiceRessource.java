/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Employe;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.EmployeService;

/**
 *
 * @author Dave
 */
@Path("/employeService")
public class EmployeServiceRessource {
    EmployeService empServ = new EmployeService();
    
    @POST
    @Path("/employeService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addEmploye(@PathParam("e") Employe e) {
        
        empServ.ajouter(e);
    }
    
    @POST
    @Path("/employeService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateEmploye(@PathParam("e") Employe e) {
        
        empServ.modifier(e);
    }
    
    @GET
    @Path("/employeService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Employe addEmploye(@PathParam("id") Integer id) {
        
        return empServ.trouver(id);
    }
    
    @DELETE
    @Path("/employeService/delete")
    public void deleteEmploye(@PathParam("id") Integer id) {
        
        empServ.supprimer(id);
    }
    
    @DELETE
    @Path("/employeService/delete")
    public void deleteEmploye(@PathParam("e") Employe e) {
        
        empServ.supprimer(e);
    }
    
    @GET
    @Path("/employeService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Employe> getListe() {
        
        return empServ.lister();
    }
    
    @GET
    @Path("/employeService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Employe> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return empServ.lister(debut, nombre);
    }
    
}

