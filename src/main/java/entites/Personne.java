/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Dave
 */
public class Personne {
    long id;
    protected String nom;
    protected String prenoms;
    private LocalDate dateNaissance;
    
    //Constructor
    Personne(long id, String nom, String prenoms, LocalDate dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenoms = prenoms;
        this.dateNaissance = dateNaissance;
    }
    
    public Personne() {
        this.nom = "";
    }
    //GETTERS
    public int getAge() {
        Date date = new Date();
        int age = date.getYear() - this.dateNaissance.getYear();
        return age;
    }
    public int getAge(LocalDate dateReference) {
        return dateReference.getYear() - this.dateNaissance.getYear();
    }
    public long getId() {
        return this.id;
    }
    
    //toString method
    @Override
    public String toString() {
        return "Personne " + nom + " " + prenoms + "né le" + dateNaissance + " avec comme identifiant" + id ;
    }
    
    //Equals method for id
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne personne = (Personne) obj;
        return this.id == personne.id;
    }
    
    //Hash code
    @Override
    public int hashCode() {
        int hash = 4;
        hash = 21 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 21 * hash + Objects.hashCode(this.nom);
        hash = 21 * hash + Objects.hashCode(this.prenoms);
        hash = 21 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }
    

}
