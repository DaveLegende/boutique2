/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class Client extends Personne{
    private String cin;
    private String carteVisa;
    
    public Client(long id, String nom, String prenoms, LocalDate dateNaissance, String cin, String carteVisa) {
        super(id, nom, prenoms, dateNaissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }
    
    public Client() {
        super();
    }
    
    @Override
    public String toString() {
        return "Client au carte de Visa : " + carteVisa + "avec cin : " + cin ;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.carteVisa);
        hash = 73 * hash + Objects.hashCode(this.cin);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client client = (Client) obj;
        if (!Objects.equals(this.carteVisa, client.carteVisa)) {
            return false;
        }
        if (!Objects.equals(this.cin, client.cin)) {
            return false;
        }
        return true;
    }

 
}
