/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class Categorie {
    private int id;
    private String libelle;
    private String description;
    List<Produit> listProduit;

    public Categorie() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int getId() {
        return this.id;
    }
    
    Categorie(int id, String libelle, String description) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
    }
    
    @Override
    public String toString() {
        return "Categorie " + id + "sous libelle : " + libelle + "(" + description + ')';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categorie categorie = (Categorie) obj;
        if (this.id != categorie.id) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 4;
        hash = 43 * hash + Objects.hashCode(this.id);
        hash = 43 * hash + Objects.hashCode(this.libelle);
        hash = 43 * hash + Objects.hashCode(this.description);
        return hash;
    }
}
